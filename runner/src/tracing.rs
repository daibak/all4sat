use std::fs::File;

use anyhow::{Result, Context};
use tracing_core::LevelFilter;
use tracing_subscriber::fmt;
use tracing_subscriber::prelude::*;
use tracing_subscriber::registry;

use crate::config;

pub fn setup(config: &config::Config) -> Result<()> {
    let filter = Into::<LevelFilter>::into(config.verbosity);

    // configure a custom event formatter
    let fmt_layer = fmt::layer()
        .with_level(true) // don't include levels in formatted output
        .without_time()
        // .with_target(false) // don't include targets
        // .with_thread_ids(false) // include the thread ID of the current thread
        // .with_thread_names(false) // include the name of the current thread
        .compact() // use the `Compact` formatting style.
        .with_filter(filter);

    let mut layers = vec![fmt_layer.boxed()];

    // file layer
    if let Some(log_file) = &config.log_file {
        let file = File::create(log_file).context("invalid path for log file, cannot create file")?;
        layers.push(
            fmt::layer()
                .with_level(true)
                .without_time()
                .with_ansi(false)
                .pretty()
                .with_writer(file)
                .with_filter(filter)
                .boxed(),
        );
    }

    // construct a tracing subscriber that prints formatted traces to stdout and init it
    registry().with(layers).init();
    Ok(())
}
