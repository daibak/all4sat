use std::path::PathBuf;

use clap::{Parser, Subcommand, ValueEnum};

#[derive(Debug, Clone, Copy, ValueEnum)]
pub enum TraceLevel {
    Off,
    Info,
    Debug,
    Trace,
}

#[derive(Parser, Debug)]
#[command(name = "Simulation Launcher")]
#[command(version, about)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Command,
    /// Set tracing level
    #[arg(short, long, default_value = "info")]
    pub verbosity: TraceLevel,
    /// Log file path
    #[arg(short, long, value_name = "LOG_FILE")]
    pub log_file: Option<PathBuf>,
    /// Sets a custom config file
    #[arg(short, long, group = "all-config", value_name = "CONFIG_FILE")]
    pub config: Option<PathBuf>,
    /// Load serialized GameConfig
    #[arg(
        short,
        long,
        conflicts_with = "config",
        value_name = "PROBLEM_FILE"
    )]
    pub problem: Option<PathBuf>,
    /// Load serialized AgentConfig
    #[arg(
        short,
        long,
        conflicts_with = "config",
        value_name = "SOLVER_CONFIG_FILE"
    )]
    pub solver_config: Option<PathBuf>,
    /// Seed for game config
    #[arg(short, long, group = "all-config", value_name = "SEED_CONFIG")]
    pub seed_config: Option<u64>,
}

#[derive(Subcommand, Debug)]
pub enum Command {
    /// Solve problem
    Solve,
    /// Print full config for convenience and reproducibility
    PrintConfig,
}


pub fn parse_cli() -> Cli {
    Cli::parse()
}
