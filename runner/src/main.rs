use anyhow::Result;

mod cli;
mod config;
mod runner;
mod tracing;

fn main() -> Result<()> {
    let cli = cli::parse_cli();
    let config = config::load(&cli)?;
    println!("{config:#?}");

    tracing::setup(&config)?;

    match &cli.command {
        cli::Command::Solve => {
            let results = runner::solve(config.problem, config.solver_config);
            println!("{results}");
        }
        cli::Command::PrintConfig => {
            std::fs::write("config_all.yaml", serde_yaml::to_string(&config).unwrap())
                .expect("local dir should be writeable to save in config.yaml");
        }
    }
    Ok(())
}
