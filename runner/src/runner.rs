use tracing::{debug, info};

use sat;
use solvers::solver;

pub fn solve(problem_def: sat::ProblemDef, solver_config: solver::Config) -> sat::Results {
    let mut solver = solver_config.build(&problem_def);
    let mut problem = sat::Problem::from_config(problem_def);
    solve(&mut problem, solver.as_mut())
}

fn inner_solve<T: solver::Solver + ?Sized>(problem: &mut sat::Problem, solver: &mut T) -> sat::Results {
    solver.solve(problem);
    debug!("\n{problem}");
    let results = solver.results();
    info!("Solver ended with {results}");
    results
}
