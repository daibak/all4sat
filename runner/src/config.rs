use std::{fs, path::PathBuf};

use anyhow::{Context, Result};
use figment::{
    providers::{Format, Serialized, Yaml},
    Figment,
};
use serde::{Deserialize, Serialize};
use tracing_core::LevelFilter;

use crate::cli::{self, Cli};
use sat::ProblemDef;
use solvers::solver;



#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum TraceLevel {
    Off,
    Info,
    Debug,
    Trace,
}

impl From<&cli::TraceLevel> for TraceLevel {
    fn from(v: &cli::TraceLevel) -> TraceLevel {
        match v {
            cli::TraceLevel::Off => TraceLevel::Off,
            cli::TraceLevel::Info => TraceLevel::Info,
            cli::TraceLevel::Debug => TraceLevel::Debug,
            cli::TraceLevel::Trace => TraceLevel::Trace,
        }
    }
}

impl From<TraceLevel> for LevelFilter {
    fn from(v: TraceLevel) -> LevelFilter {
        match v {
            TraceLevel::Off => LevelFilter::OFF,
            TraceLevel::Info => LevelFilter::INFO,
            TraceLevel::Debug => LevelFilter::DEBUG,
            TraceLevel::Trace => LevelFilter::TRACE,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub verbosity: TraceLevel,
    pub log_file: Option<PathBuf>,
    pub problem: ProblemDef,
    pub solver_config: solver::Config,
}

// The default configuration.
impl Default for Config {
    fn default() -> Self {
        Config {
            verbosity: TraceLevel::Info,
            log_file: None,
            problem: ProblemDef::new_random(),
            solver_config: solver::Config::CDCL(),
        }
    }
}

fn des_yaml_file<T: for<'a> Deserialize<'a>>(path: &PathBuf, name: &str) -> Result<T> {
    serde_yaml::from_str(
        fs::read_to_string(path)
            .context(format!("invalid path for {name}"))?
            .as_str(),
    )
    .context(format!("invalid {name} in yaml"))
}

pub fn load(cli: &Cli) -> Result<Config> {
    let base_config = Figment::new()
        .merge(Serialized::defaults(Config::default()))
        .merge(("verbosity", TraceLevel::from(&cli.verbosity)))
        .merge(("log_file", &cli.log_file));

    let figment = match (
        &cli.config,
        &cli.problem,
        &cli.solver_config,
        &cli.seed_config,
    ) {
        (Some(config_path), None, None, None) => base_config.merge(Yaml::file(config_path)),
        (None, Some(problem_path), None, _) => {
            let problem: ProblemDef = des_yaml_file(problem_path, "sat problem")?;
            base_config.merge(("problem", problem))
        }
        (None, None, Some(solver_config_path), None) => {
            let solver_config: solver::Config = des_yaml_file(solver_config_path, "solver config")?;
            base_config.merge(("solver_config", solver_config))
        }
        (None, None, Some(solver_config_path), Some(seed)) => {
            let problem = ProblemDef::new_random_with_seed(*seed);
            let solver_config: solver::Config = des_yaml_file(solver_config_path, "solver config")?;
            base_config
                .merge(("problem", problem))
                .merge(("solver_config", solver_config))
        }
        (None, Some(problem_path), Some(solver_config_path), _) => {
            let problem: ProblemDef = des_yaml_file(problem_path, "sat problem")?;
            let solver_config: solver::Config = des_yaml_file(solver_config_path, "solver config")?;
            base_config
                .merge(("problem", problem))
                .merge(("solver_config", solver_config))
        }
        (None, None, None, Some(seed)) => {
            let problem = ProblemDef::new_random_with_seed(*seed);
            base_config.merge(("problem", problem))
        }
        (None, None, None, None) => base_config,
        _ => unreachable!(),
    };

    Ok(figment.extract()?)
}
