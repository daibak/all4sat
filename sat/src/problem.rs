use std::fmt;

pub struct Problem {}

pub struct Results {
    satisfied: bool,
}
impl fmt::Display for Results {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SAT={}", self.satisfied)
    }
}
